# Aufgabe_3_3

## Erzeuger-Verbraucher mit Java-Threads

Realisieren Sie eine Lösung für das Erzeuger-Verbraucher Problem mit Hilfe von Java Threads,
indem Sie jeweils eine Klasse für den Erzeuger und für den Verbraucher erstellen. In einer weiteren
Klasse soll dann jeweils ein Erzeuger und ein Verbraucher gestartet werden. Schauen Sie sich dazu
nochmals die JavaDoc von Threads an!

- Laden Sie zuerst den Code mit dem Ringpuffer von der Webseite herunter.
- Erstellen Sie die benötigten Klassen unter Verwendung von JAVA Threads
- Ist diese Lösung frei von Deadlook? Begründen Sie ihre Antwort!
- Erzeugen Sie jeweils zwei Erzeuger bzw. Verbraucher Instanzen und führen Sie das

Programm nochmals aus! Was beobachten Sie? Ist die Lösung frei von Verklemmungen?
