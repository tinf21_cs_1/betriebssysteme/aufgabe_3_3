public class Erzeuger extends Thread {

    public int nummer;

    public Erzeuger(int nummer) {
        this.nummer = nummer;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("Erzeuger " + nummer + ": " + i);
            Main.rb.insertItem(1);
        }
    }
}
