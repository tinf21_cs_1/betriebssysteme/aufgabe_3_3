public class Verbraucher extends Thread {

    public int nummer;

    public Verbraucher(int nummer) {
        this.nummer = nummer;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("Verbraucher " + nummer + ": " + i);
            Main.rb.removeItem();
        }
    }
}
