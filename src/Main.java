import util.RingBuffer;

public class Main {

    public static RingBuffer rb;
    public static Erzeuger e1;
    public static Erzeuger e2;
    public static Verbraucher v1;
    public static Verbraucher v2;

    public static void main(String[] args) {

        rb = new RingBuffer(5);

        e1 = new Erzeuger(1);
        e2 = new Erzeuger(2);
        v1 = new Verbraucher(1);
        v2 = new Verbraucher(2);

        e1.start();
        //e2.start();
        v1.start();
        v2.start();

    }
}